# DCGAN Anime Faces

**The goal of this project is to create new faces of anime characters using a Deep Convolutional Generative Adversarial Network (DCGAN).**

## Generative Adversarial Networks

The objective of a GAN is to train a data generator in order to imitate a given dataset.
<br/>A GAN is similar to a zero sum game between two neural networks, the generator of data and a discriminator, trained to recognize original data from fakes created by the generator.

<div align="center">
![alt text](images/GAN_structure.png "Structure of a GAN")
</div>

At each step, the discriminator is trained on a batch containing real and fake images. The generator is then trained to produce a batch of images.

<br/>**In order to create effective GANs on images, we must use convolutional layers in the discriminator and in the generator.**

## DCGAN

In a Deep Convolutional GAN, the data generator has the following structure :

<div align="center">
![alt text](images/DCGAN_structure.png "Structure of a DCGAN")
</div>

It takes a noise vector as the input in order to diversify the potential outputs.
This vector is reshaped into a structure with an important number of channels.
Then, a succession of convolutional layers will reduce the depth and create pattern in the other dimensions (height and width) until we obtain the output, composed of 3 channels (for a colorized image).
<br/>Ideally, after the training each dimension will correspond to a feature of the image, for example the hair color of the character.
<br/>With a simple perceptron to generate the image it is way harder to regularly obtain multi pixel structures such as the eyes of the characters.
<br/>The discriminator is a simple CNN, flexible enough for a problem of this complexity.

## Things to keep in mind while training a GAN

**In the case of a GAN, cost functions may not converge using gradient descent.**
<br/>Training a GAN is very different from training a regular neural network. The main difference come from the fact that the discriminator also evolves.
Therefore, the cost function that the generator tries to minimize is also evolving.

**The discriminator must be powerful enough to recognize small differences between the real and fake images.**
<br/>If the discriminator does not perform well, the generator cannot progress.
If the accuracy of the discriminator stabilizes near 50%, the generator already tricks the discriminator well and does not need to improve.

**The two networks must progress in synergy to avoid a diminished gradient during the training of the generator.**
<br/>The discriminator must not be too effective at the beginning, if it is, the generator will not progress until it randomly creates a very convincing image.

We want to avoid **mode collapse** which is when the generator creates the same output no matter the input noise.
It leads to an overfitting from the discriminator which remembers the features of the fake, no matter how convincing it is.

**A noise vector of dimension 100 seems to be a good heuristic.**
<br/>Dimension of the noise must be high enough to allow many values for many features (in our case hair color, eyes size/color…).
However, a large number like 1000 makes it complicated for the network to map those to the different features, making the training much slower.
It is recommended to use a vector of size 512 for some more complex GANs, which is the maximum number of channels in the convolutions.

**A high learning rate creates a situation in which both networks overfit to exploit short term opponent weaknesses.**
<br/>The **learning rate** of the optimizer must be carefully chosen.
<br/>A high learning rate prevents the generator from reaching a certain level of details, it prevents the convergence.
It also leads to an overfitting from the discriminator which recognizes the most recent features made by the generator but forgets the previous ones.
However, the learning rate must be high enough for the generator to adapt quickly, as a slow learning leads to many similar fake images for the discriminator to train on.

According to [1] the default choice for the exponential decay rate for the first moment estimate leads to instability.
I used the suggested value of 0.5.

## Structure of the networks

### The Generator

<div align="center">
<img src="images/generator.png"  width="260" height="500">
</div>

### The Discriminator

<div align="center">
<img src="images/discriminator.png"  width="400" height="500">
</div>

## Some results

Some of the images in the dataset :
<div align="center">
![alt text](images/training_images.png "Images from the dataset")
</div>

Here are some of the images produced by the generator after being trained with 15000 batches :
<div align="center">
![alt text](images/Faces_16800.png "Images generated")
![alt text](images/Faces_18000.png "Images generated")
</div>

A few images are convincing and those images present different hair styles, hair colors or face orientations.
<br/>We avoided mode collapse and the networks seem to have a good synergy with a stable accuracy at around 80% for the discriminator during most of the training.
<br/>Howerver, the generator does not create globally convincing faces, for example some of them present different shape and colors between the two eyes.


### Reference
Unsupervised representation learning with Deep Convolutional Generative Adversarial Networks - Alec Radford, Luke Metz and Soumith Chintala https://arxiv.org/pdf/1511.06434.pdf [1]